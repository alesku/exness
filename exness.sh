#!/bin/bash

errCheck(){
    if [ "$1" -ne 0 ]; then
        echo -e "[ERROR]: Failed to execute step \"$2\""
        return 1
    fi
}

# Cluster name should be unique, lets add some random to ephemeral machine
sudo apt-get install -y uuid-runtime
errCheck $? "Installing uuid-runtime"
UUID=$(uuidgen | cut -c1-8)

# Assume we are working within existing project
# DEVSHELL_PROJECT_ID=${DEVSHELL_PROJECT_ID}
CLUSTER_NAME=awsm-php-cluster-${UUID}

# Creating cluster
gcloud beta container \
  --project "${DEVSHELL_PROJECT_ID}" clusters create "${CLUSTER_NAME}" \
  --zone "us-central1-a" \
  --no-enable-basic-auth \
  --cluster-version "1.15.4-gke.22" \
  --machine-type "n1-standard-1" \
  --image-type "COS" \
  --disk-type "pd-standard" \
  --disk-size "100" \
  --metadata disable-legacy-endpoints=true \
  --scopes "https://www.googleapis.com/auth/devstorage.read_only","https://www.googleapis.com/auth/logging.write","https://www.googleapis.com/auth/monitoring","https://www.googleapis.com/auth/servicecontrol","https://www.googleapis.com/auth/service.management.readonly","https://www.googleapis.com/auth/trace.append" \
  --num-nodes "3" \
  --enable-stackdriver-kubernetes \
  --enable-ip-alias \
  --network "projects/${DEVSHELL_PROJECT_ID}/global/networks/default" \
  --subnetwork "projects/${DEVSHELL_PROJECT_ID}/regions/us-central1/subnetworks/default" \
  --default-max-pods-per-node "110" \
  --addons HorizontalPodAutoscaling,HttpLoadBalancing \
  --enable-autoupgrade \
  --enable-autorepair

errCheck $? "Deploying cluster"

# Lets connect to cluster now
gcloud container clusters get-credentials ${CLUSTER_NAME} \
  --zone us-central1-a \
  --project ${DEVSHELL_PROJECT_ID}


errCheck $? "Get cluster credentials"


for definition in `ls php-app` ; do 
	kubectl apply -f php-app/$definition ; 
	errCheck $? "Apply definition $definition"
done

kubectl expose deployment nginx --port=80 --type=LoadBalancer
errCheck $? "Exposing nginx port"

# Lets get a cluster IP to check
retryCount=0
while [ $retryCount -lt 50 ]; do
	CLUSTER_IP=$(kubectl get svc nginx | grep nginx | awk {'print $4'})

    if [[ $CLUSTER_IP =~ ^[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+$ ]]; then 
        echo "Getting cluster IP. Iteration: $retryCount" # The Printed Message
        echo "Cluster IP: ${CLUSTER_IP}"
        break 
    fi
 
    echo "Getting cluster IP. Iteration: $retryCount" # The Printed Message
    retryCount=$(($retryCount + 1))
        if [[ $retryCount -eq 50 ]]; then 
            echo "Something is wrong with cluster, guess, I'll die ..."
            return 1
            break 
        fi
    echo "Cluster IP: ${CLUSTER_IP}"
    sleep 5
done



# Checking cluster finally: polling HTTP endpoint
retryCount=0
while [ $retryCount -lt 50 ]; do
  HTTP_CODE=$(curl -s -o /dev/null -w "%{http_code}"  ${CLUSTER_IP})

    if [[ $HTTP_CODE -eq 200 ]]; then 
         
        echo "Checking cluster health. Iteration: $retryCount"
        echo "Cluster successfully deployed, available by ${CLUSTER_IP}"
        curl -i ${CLUSTER_IP}
        echo 
        break 
    fi
 
    echo "Checking cluster health. Iteration: $retryCount"
    retryCount=$(($retryCount + 1))
        if [[ $retryCount -eq 50 ]]; then 
            echo "Something is wrong with cluster, guess, I'll die ..."
            return 1
            break 
        fi
    echo "Cluster is not ready yet, retry in 5 seconds ..."
    sleep 5
done
